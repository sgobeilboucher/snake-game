using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Grid : MonoBehaviour
{
    public GameObject prefabCell;
    public RectTransform parentCell;

    private Cell[][] cells;
    //size of the cell change to be all in the screen
    private float cellSize;
    //Psition 0.0 is the middle of the screen; offset needed to make bottom right cell[0,0]
    private float offsetX;
    private float offsetY;

    public float CellSize => cellSize;

    private void Awake()
    {
        //set size of the grid/cell
        cellSize = GetSizeCell();
        float sizeHor = GameOptions.GRID_WIDTH * cellSize;
        float sizeVert = GameOptions.GRID_HEIGHT * cellSize;
        offsetX = sizeHor / 2f;
        offsetY = sizeVert / 2f;
        parentCell.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, sizeHor);
        parentCell.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, sizeVert);

        //create cells
        cells = new Cell[GameOptions.GRID_WIDTH][];
        for (int i = 0; i < GameOptions.GRID_WIDTH; i++)
        {
            cells[i] = new Cell[GameOptions.GRID_HEIGHT];
            for (int j = 0; j < GameOptions.GRID_HEIGHT; j++)
            {
                cells[i][j] = CreateNewCell(i,j);
            }
        }
    }

    private Cell CreateNewCell(int _x, int _y)
    {
        Vector2 grid_pos = new Vector2(_x, _y);
        Transform obj = Instantiate(prefabCell).transform;
        obj.SetParent(parentCell);
        obj.localPosition = new Vector3(cellSize * _x - offsetX, cellSize * _y - offsetY, 0f);
        Cell cell = obj.GetComponent<Cell>();
        cell.Setup(grid_pos,cellSize);

        return cell;
    }

    private float GetSizeCell()
    {
        //get size max so that all the grid is visible
        float maxWidth = (float)Screen.width / GameOptions.GRID_WIDTH;
        float maxHeight = (float)Screen.height / GameOptions.GRID_HEIGHT;

        return Mathf.Min(maxWidth, maxHeight);
    }

    //get position for cell
    public Vector3 GetPositionCell(Vector2 _gridPos)
    {
        Cell c = GetCell(_gridPos);
        if (c != null)
        {
            return c.transform.localPosition;
        }
        return Vector3.zero;
    }

    public Cell GetCell(Vector2 _gridPos)
    {
        int x = (int)_gridPos.x;
        int y = (int)_gridPos.y;
        if (x < cells.Length && y < cells[x].Length)
        {
            return cells[x][y];
        }

        return null;
    }

    //Get all the cell not ocupped and choose one at random for the next apple
    public void SpawnRandomApple()
    {
        List<Cell> optionCells = new List<Cell>();
        for (int i = 0; i < cells.Length; i++)
        {
            for (int j = 0; j < cells[i].Length; j++)
            {
                Cell c = cells[i][j];
                if (!c.occuped)
                {
                    optionCells.Add(c);
                }
            }
        }

        int random = Random.Range(0, optionCells.Count-1);
        optionCells[random].ActivateApple(true);
    }
}