using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnakeCell : MonoBehaviour
{
    //Link img to change the color
    public Image img;

    private RectTransform transform;
    private Vector2 gridPosition;
    private Vector2 lastGridPosition;

    private Cell currentCell;
    private Player player;
    private PlayersManager manager;

    public Vector2 LastGridPos => lastGridPosition;
    public Vector2 GridPos => gridPosition;

    public void Setup(PlayersManager _manager,Player _player, Vector2 _gridPos, Color _color, float _size)
    {
        manager = _manager;
        player = _player;
        transform = GetComponent<RectTransform>();
        transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _size);
        transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _size);

        gridPosition = _gridPos;
        lastGridPosition = _gridPos;
        img.color = _color;

        currentCell = manager.grid.GetCell(_gridPos);
        currentCell.occuped = true;
    }

    //Move to new grid cell
    public bool Move(Vector2 _gridPos)
    {
        currentCell.occuped = false;
        //Game end for player if touch edge of grid
        if (_gridPos.x >= GameOptions.GRID_WIDTH || _gridPos.x < 0 ||
            _gridPos.y >= GameOptions.GRID_HEIGHT || _gridPos.y < 0)
        {
            return false;
        }

        currentCell = manager.grid.GetCell(_gridPos);

        //Game end for player if grid cell ocupped by another snake
        if (currentCell.occuped)
        {
            return false;
        }

        //Player snake grow if apple on the cell
        if (currentCell.AppleActive)
        {
            currentCell.ActivateApple(false);
            player.Grow();
        }

        lastGridPosition = gridPosition;
        gridPosition = _gridPos;
        transform.localPosition = manager.grid.GetPositionCell(gridPosition);

        currentCell.occuped = true;
        return true;
    }

    public void OnDestroy()
    {
        if(currentCell != null) currentCell.occuped = false;
    }
}