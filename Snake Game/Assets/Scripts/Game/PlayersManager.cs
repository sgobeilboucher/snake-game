using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Create the players and validate if lose the game
public class PlayersManager : MonoBehaviour
{
    public enum Direction
    {
        Left,
        Up,
        Right,
        Down
    }

    //Colors and controls for each player are on scriptableObjects (Assets/Datas/Players)
    public List<PlayerData> PlayerDatas;
    //Initial size of a player snake
    public int snakeSize = 6;
    //Time between each update
    public float gameSpeed = 0.2f;
    //no system to manage ui in this small prototype so the popups are link here
    public GameOverPopup goPopup;
    public GameOverPopup congratsPopup;
    public Grid grid;
    public GameObject prefabPlayer;

    private List<Player> players = new List<Player>();
    private List<Vector2> startPos = new List<Vector2>();

    private float currentTime;
    private bool gameEnded = false;

    private void Start()
    {
        //game start when the scene is loaded
        currentTime = gameSpeed;
        //player starting position
        float xJump = (float)GameOptions.GRID_WIDTH / (GameOptions.PLAYERS + 1);
        int y = Mathf.Min(Mathf.RoundToInt(GameOptions.GRID_HEIGHT / 2f), 6);
        for (int i = 0; i < GameOptions.PLAYERS; i++)
        {
            startPos.Add(new Vector2(Mathf.RoundToInt(xJump * (i+1)), y));
        }

        //Create players
        for (int i = 0; i < GameOptions.PLAYERS; i++)
        {
            players.Add(CreatePlayer(i));
        }

        //spawn first apple
        grid.SpawnRandomApple();
    }

    private void Update()
    {
        if (!gameEnded)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0f)
            {
                foreach (Player player in players)
                {
                    if (!player.Move())
                    {
                        break;
                    }
                }

                currentTime = gameSpeed;
            }
        }
    }

    private Player CreatePlayer(int _playerID)
    {
        Transform obj = Instantiate(prefabPlayer).transform;
        obj.SetParent(transform);
        obj.localPosition = Vector3.zero;
        Player player = obj.GetComponent<Player>();
        player.Init(_playerID, this, startPos[_playerID]);

        return player;
    }

    public void OnPlayerDead(Player _player)
    {
        players.Remove(_player);
        Destroy(_player.gameObject);
        if (players.Count == 0)
        {
            gameEnded = true;
            goPopup.ShowPopup();
        }
    }

    public void OnPlayerWin()
    {
        gameEnded = true;
        congratsPopup.ShowPopup();
    }
}