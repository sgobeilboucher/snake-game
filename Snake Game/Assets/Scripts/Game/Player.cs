using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject prefabSnakeCell;

    private List<SnakeCell> snakeCells = new List<SnakeCell>();
    private PlayersManager manager;
    private PlayersManager.Direction direction = PlayersManager.Direction.Down;
    private PlayerData data;

    private int maxSize;
    private int playerId;

    public void Init(int _playerId, PlayersManager _manager, Vector2 _headPos)
    {
        playerId = _playerId;
        manager = _manager;
        data = manager.PlayerDatas[playerId];

        //Get snake size to win the game
        maxSize = GameOptions.GRID_WIDTH * GameOptions.GRID_HEIGHT;

        //Snake head got a different color
        bool isHead = true;
        float gridPosY = _headPos.y;
        for (int i = 0; i < _manager.snakeSize; i++)
        {
            snakeCells.Add(CreateCellSnake(_headPos,i,isHead));
            isHead = false;
            //snake start vertical with the 'head' on the bottom
            gridPosY += 1f;
            _headPos = new Vector2(_headPos.x, gridPosY);
        }

        //Add component for the control (only keyboard for now)
        switch (data.controlType)
        {
            default:
            case PlayerData.ControlType.Keyboard:
                gameObject.AddComponent<KeyboardControl>().AttachPlayer(this, data);
            break;
        }
    }

    //called from the appropriate control script
    public void ChangeDirection(PlayersManager.Direction _direction)
    {
        //can't go opposite direction
        if ((direction == PlayersManager.Direction.Left && _direction == PlayersManager.Direction.Right) ||
            (direction == PlayersManager.Direction.Right && _direction == PlayersManager.Direction.Left) ||
            (direction == PlayersManager.Direction.Up && _direction == PlayersManager.Direction.Down) ||
            (direction == PlayersManager.Direction.Down && _direction == PlayersManager.Direction.Up))
        {
            return;
        }

        direction = _direction;
    }

    //Called at start and when snake eat an apple
    private SnakeCell CreateCellSnake(Vector2 _gridPos, int id, bool _head)
    {
        Transform obj = Instantiate(prefabSnakeCell).transform;
        obj.SetParent(transform);
        obj.localPosition = manager.grid.GetPositionCell(_gridPos);
        SnakeCell cell = obj.GetComponent<SnakeCell>();
        cell.Setup(manager,this, _gridPos, _head ? data.headColor : data.playerColor, manager.grid.CellSize);
        //Debug.Log("<color=#"+ColorUtility.ToHtmlStringRGBA(data.playerColor)+"> cell["+id+"] to "+_gridPos.x+" / " + _gridPos.y+"</color>");
        return cell;
    }

    //Snake eat an apple
    public void Grow()
    {
        manager.grid.SpawnRandomApple();
        snakeCells.Add(CreateCellSnake(snakeCells[snakeCells.Count-1].LastGridPos,snakeCells.Count-1,false));
        //Win condition
        if (snakeCells.Count == maxSize)
        {
            manager.OnPlayerWin();
        }
    }

    public bool Move()
    {
        for (int i = 0; i < snakeCells.Count; i++)
        {
            Vector2 pos;
            //only head need to calcul the nest position
            if (i == 0)
            {
                Vector2 current = snakeCells[i].GridPos;
                switch (direction)
                {
                    case PlayersManager.Direction.Left:
                        pos = new Vector2(current.x-1, current.y);
                        break;
                    case PlayersManager.Direction.Up:
                        pos = new Vector2(current.x, current.y+1);
                        break;
                    case PlayersManager.Direction.Right:
                        pos = new Vector2(current.x+1, current.y);
                        break;
                    default:
                        pos = new Vector2(current.x, current.y-1);
                        break;
                }
            }
            else
            {
                //all other snake call get the last position from the cell in front of them
                pos = snakeCells[i - 1].LastGridPos;
            }

            //if can't move = lose the game
            if (!snakeCells[i].Move(pos))
            {
                GameOver();
                return false;
            }
        }

        return true;
    }

    //Game over for the player
    private void GameOver()
    {
        foreach (SnakeCell cell in snakeCells)
        {
            cell.gameObject.SetActive(false);
        }
        manager.OnPlayerDead(this);
    }
}