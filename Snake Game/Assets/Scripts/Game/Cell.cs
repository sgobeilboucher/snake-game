using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    //ocuuped by a snake
    public bool occuped = false;
    public Vector2 gridPosition;
    public GameObject appleImg;

    private RectTransform transform;
    //Apple is directly in the cell
    private bool appleActive = false;
    public bool AppleActive => appleActive;

    private void Awake()
    {
        ActivateApple(false);
    }

    //size of the cell change to be all in the screen
    public void Setup(Vector2 _grid_pos, float _size)
    {
        gridPosition = _grid_pos;

        transform = GetComponent<RectTransform>();
        transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _size);
        transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _size);

        transform.name = "Cell[" + gridPosition.x + "/" + gridPosition.y + "]";
    }

    public void ActivateApple(bool _active)
    {
        appleActive = _active;
        appleImg.SetActive(_active);
    }
}