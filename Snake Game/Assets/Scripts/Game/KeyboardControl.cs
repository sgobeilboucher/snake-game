using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Key binds are set on the scriptableObject of the playerData (Assets/Data/Players)
public class KeyboardControl : MonoBehaviour
{
    private Player player;
    private PlayerData playerData;

    //Component attach by the player
    public void AttachPlayer(Player _player, PlayerData _playerData)
    {
        player = _player;
        playerData = _playerData;
    }
    private void Update()
    {
        if (Input.GetKeyDown(playerData.keyMoveLeft) )
        {
            player.ChangeDirection(PlayersManager.Direction.Left);
        }

        if (Input.GetKeyDown(playerData.keyMoveUp))
        {
            player.ChangeDirection(PlayersManager.Direction.Up);
        }

        if (Input.GetKeyDown(playerData.keyMoveRight))
        {
            player.ChangeDirection(PlayersManager.Direction.Right);
        }

        if (Input.GetKeyDown(playerData.keyMoveDown))
        {
            player.ChangeDirection(PlayersManager.Direction.Down);
        }
    }
}