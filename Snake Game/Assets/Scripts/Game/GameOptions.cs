using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Options that can be set on the main window before starting a game
public class GameOptions
{
    public static int PLAYERS = 2;
    public static int GRID_WIDTH = 30;
    public static int GRID_HEIGHT = 20;
}