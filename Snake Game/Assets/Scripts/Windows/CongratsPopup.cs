using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Popup when we win the game
public class CongratsPopup : MonoBehaviour
{
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void ShowPopup()
    {
        gameObject.SetActive(true);
    }

    public void OnClickQuit()
    {
        SceneManager.LoadScene("Launcher");
    }
}