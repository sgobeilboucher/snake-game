using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Popup when we lose the game
public class GameOverPopup : MonoBehaviour
{
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void ShowPopup()
    {
        gameObject.SetActive(true);
    }

    public void OnClickQuit()
    {
        SceneManager.LoadScene("Launcher");
    }
}