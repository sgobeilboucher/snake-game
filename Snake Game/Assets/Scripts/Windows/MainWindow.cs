using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainWindow : MonoBehaviour
{
    [Header("Players")]
    public Text playerTxt;
    public Slider playerSlider;

    [Header("Grid Width")]
    public Text gridWidthTxt;
    public Slider gridWidthSlider;

    [Header("Grid Height")]
    public Text gridHeightTxt;
    public Slider gridHeightSlider;

    private void Start()
    {
        OnSliderValueChange();
    }

    public void OnSliderValueChange()
    {
        playerTxt.text = "Players: " + playerSlider.value;
        gridWidthTxt.text = "Grid Width: " + gridWidthSlider.value;
        gridHeightTxt.text = "Grid Height: " + gridHeightSlider.value;
    }
    public void OnClickStart()
    {
        GameOptions.PLAYERS = Mathf.RoundToInt(playerSlider.value);
        GameOptions.GRID_HEIGHT = Mathf.RoundToInt(gridHeightSlider.value);
        GameOptions.GRID_WIDTH = Mathf.RoundToInt(gridWidthSlider.value);
        SceneManager.LoadScene("Game");
    }
}