using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class PlayerData : ScriptableObject
{
    //possible control types
    public enum ControlType
    {
        Keyboard,
        Mouse,
        Controller,
        Touch
    }

    public Color playerColor;
    public Color headColor;

    public ControlType controlType;

    [Header("Keyboard Control")]
    public KeyCode keyMoveLeft = KeyCode.LeftArrow;
    public KeyCode keyMoveRight = KeyCode.RightArrow;
    public KeyCode keyMoveUp = KeyCode.UpArrow;
    public KeyCode keyMoveDown = KeyCode.DownArrow;

    //more settings for each control type can be here
}