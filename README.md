# Snake Game

Making this prototype took me about 7 hours. A lot can be improved and possibly better solution could have been chosen for this game.

## How my code would be able to deal with:

Change the size of the board, or possibly make in infinitely big

```
Available on the main window to change (no infinity available in this prototype)
```

Change the control schemes for each player (different keys, mouse, touch)

```
Can be change by player on their data 'Assets/Datas/Player' (Only Keyboard implemented)
```

Add many snake players

```
1 to 4 available by the main window (more can be added by adding new playerDatas to PlayerManager)
```

Add AI in the future

```
I would add a new 'control' AI who will analize if changing direction is better
```

Add replays in the future

```
Each played could keep a array with the direction change+time of change and a array of positions for the apples can be saved.
With those infos we can recreate the game.
```

## Notes

-UI downloaded from Asset store (https://assetstore.unity.com/packages/2d/gui/icons/sleek-essential-ui-pack-170650)

-The condition for 'winning' the game was not tested ;)



